"use strict";
(function () {
    document.addEventListener("DOMContentLoaded", () => {
        const typeList = ["natural", "enhanced", "aerosol", "cloud"];
        // modified html for this
        for (let i = 0; i < typeList.length; i++) {
            const option = document.createElement("option");
            option.value = typeList[i];
            option.text = typeList[i].charAt(0).toUpperCase() + typeList[i].slice(1);
            const dropdown = document.querySelector("#type")
            dropdown.add(option);
        }
        let EpicApplication = {
            type: null,
            date: null,
            dateCache: {
                [typeList[0]]: null,
                [typeList[1]]: null,
                [typeList[2]]: null,
                [typeList[3]]: null
            },
            imgCache: new Map([
                [typeList[0], new Map()],
                [typeList[1], new Map()],
                [typeList[2], new Map()],
                [typeList[3], new Map()]
            ])
        }
        const form = document.querySelector("form");
        const imageMenu = document.querySelector("#image-menu");
        const dropdown = document.querySelector("#type");
        
        dropdown.addEventListener("change", getRecentDate);
        getRecentDate();

        // also had to change a few things inside because it didnt feel compatible with the assignment, 40 lines i was told... now 100
        function getRecentDate() {
            const originalType = document.querySelector("#type").value;
            const dateInput = document.querySelector("#date");
            if (EpicApplication.dateCache[originalType] !== null) {
                dateInput.max = EpicApplication.dateCache[originalType];
            } else {
                fetch(`https://epic.gsfc.nasa.gov/api/${originalType}/all`).then(responce => responce.json())
                    .then(data => { 
                        EpicApplication.dateCache[originalType] = data[0].date; 
                        dateInput.max = data[0].date 
                    }).catch((error) => { console.error("NO IMAGE LINKS:", error); });
            }
        }

        // CHANGING NAMED FUNCTIONS TO ANNONYMOUS FUNCTIONS
        // also had to change a few things inside because it didnt feel compatible with the assignment
        form.addEventListener("submit", function (event) {
            event.preventDefault();
            EpicApplication.type = document.querySelector("#type").value;
            EpicApplication.date = document.querySelector("#date").value;
            const imageMenu = document.querySelector("#image-menu");
            imageMenu.textContent=undefined;
            let i = 0;
            if (EpicApplication.date !== "") {
                if (EpicApplication.imgCache[EpicApplication.type] && EpicApplication.imgCache[EpicApplication.type].has(EpicApplication.date)) {
                    EpicApplication.imgCache[EpicApplication.type].get(EpicApplication.date).forEach((dates) => {
                    const clone = document.getElementById("image-menu-item").content.cloneNode(true);
                    const newLi = clone.querySelector("li");
                    newLi.setAttribute("data-index", i);
                    newLi.querySelector(".image-list-title").textContent = dates.date;
                    document.querySelector("#image-menu").appendChild(clone);
                    i++;
                    });
                } else {
                    fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.type}/date/${EpicApplication.date}`).then((reponse) => reponse.json()).then((data) => {
                        if (!EpicApplication.imgCache[EpicApplication.type]) {
                            EpicApplication.imgCache[EpicApplication.type] = new Map();
                        }
                        EpicApplication.imgCache[EpicApplication.type].set(EpicApplication.date, data);
                        if (data.length == 0) {
                            const newLi = document.createElement("li");
                            newLi.textContent = "No Links";
                            document.querySelector("#image-menu").appendChild(newLi);
                        } else {
                            data.forEach((dates) => {
                                const clone = document.getElementById("image-menu-item").content.cloneNode(true);
                                const newLi = clone.querySelector("li");
                                newLi.setAttribute("data-index", i);
                                newLi.querySelector(".image-list-title").textContent = dates.date;
                                document.querySelector("#image-menu").appendChild(clone);
                                i++;
                            })
                        }
                    }).catch((error) => { console.error("NO LINKS ERROR:", error); });
                }
            }
        });
        // CHANGING NAMED FUNCTIONS TO ANNONYMOUS FUNCTIONS
        // also had to change a few things inside because it didnt feel compatible with the assignment
        imageMenu.addEventListener("click", (event) => {
            if (event.target.tagName === "SPAN" && event.target.innerText!="No Links" ) {
                const earthImage = document.querySelector("#earth-image");
                const footDate = document.querySelector("#earth-image-date");
                const footTitle = document.querySelector("#earth-image-title");
                const i = event.target.closest("li").getAttribute("data-index");
                const imgSOURCE = EpicApplication.imgCache[EpicApplication.type].get(EpicApplication.date)[i];
                let epicDate = EpicApplication.date;
                epicDate = epicDate.replaceAll("-", "/");
                earthImage.src = `https://epic.gsfc.nasa.gov/archive/${EpicApplication.type}/${epicDate}/jpg/${imgSOURCE.image}.jpg`
                footDate.textContent = imgSOURCE.date;
                footTitle.textContent = imgSOURCE.caption;
            } 
        });
    });
})();